import React, { ReactNode } from 'react'
import Head from 'next/head'
import 'tailwindcss/tailwind.css'

type Props = {
  children?: ReactNode
  title?: string
}

const Layout = ({ children, title = 'This is the default title' }: Props) => (
  <>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="robots" content="noindex, nofollow" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="preconnect" href="https://fonts.gstatic.com" />
      <link
        href="https://fonts.googleapis.com/css2?family=Lora:wght@600&display=swap"
        rel="stylesheet"
      />
      <link
        href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap"
        rel="stylesheet"
      />
      <style>
        {`
          .font-lora {
            font-family: Lora;
          }
        `}
      </style>
    </Head>
    <div className="mx-2 text-gray-800">
      <div className="container mx-auto max-w-screen-sm">{children}</div>
    </div>
  </>
)

export default Layout
