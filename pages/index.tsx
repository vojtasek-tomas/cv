import React from 'react'
import classNames from 'classnames'

import GithubIcon from '../assets/icons/github.svg'
import LinkedInIcon from '../assets/icons/linkedin.svg'
import TwitterIcon from '../assets/icons/twitter.svg'

import Layout from '../components/Layout'
import data from '../data.json'
import photo from '../assets/me.jpg'

type ElementProps = { children: React.ReactNode; className?: string }

const createElement = (className: string, element = 'div') => (
  props: ElementProps
) => {
  return React.createElement(element, {
    className: classNames(props.className, className),
    children: props.children
  })
}

const Summary = createElement('text-center my-3')

const Section = createElement('my-3')
const SectionTitle = createElement(
  'text-3xl font-semibold py-2 border-t-2 border-b border-gray-300 antialiased'
)
const Item = createElement('mt-4 mb-10')
const ItemCondensed = createElement('mt-2 mb-5')
const ItemTitle = createElement('text-2xl font-bold antialiased mb-1')
const ItemSubTitle = createElement(
  'text-xl font-lora font-semibold antialiased'
)
const ItemDescription = createElement('mt-2 mb-3')

const NBSP = String.fromCharCode(160)

const Text = ({ children }: { children: string }) => (
  <span>{children.replace(/~/g, NBSP)}</span>
)

const List = ({ items }: { items: string[] }) => (
  <ul className="text-sm list-inside list-disc">
    {items.map((item, i) => (
      <li className="my-1" key={i}>
        {item}
      </li>
    ))}
  </ul>
)

const IndexPage = () => {
  return (
    <Layout title={data.title}>
      <div className="text-center mt-4 mb-2">
        <img className="mb-3 mx-auto rounded-full" src={photo} width={80} />
        <h1 className="mb-1 text-4xl font-bold">{data.name}</h1>
      </div>

      <Summary>
        <div className="flex items-center justify-center">
          <a className="mx-1" href={data.socials.github} target="_blank">
            <GithubIcon className="text-gray-300 hover:text-gray-600" />
          </a>

          <a className="mx-1" href={data.socials.linkedin} target="_blank">
            <LinkedInIcon className="text-gray-300 hover:text-gray-600" />
          </a>

          <a className="mx-1" href={data.socials.twitter} target="_blank">
            <TwitterIcon className="text-gray-300 hover:text-gray-600" />
          </a>
        </div>
        <div className="my-2">
          <a
            className="cursor-pointer hover:underline text-blue-500"
            href={`mailto:${data.email}`}>
            {data.email}
          </a>
        </div>
      </Summary>

      <Section>
        <SectionTitle>About</SectionTitle>
        <Item>
          <p>
            <Text>{data.about}</Text>
          </p>
          <p className="text-sm mt-1">
            I like and learn{' '}
            <a
              className="text-gray-600 cursor-pointer hover:underline"
              href="https://github.com/vojty/feather-gb"
              target="_blank">
              Rust
            </a>{' '}
            (since 2020).
          </p>
        </Item>
      </Section>

      <Section>
        <SectionTitle>Work Experience</SectionTitle>
        {data.work.map((job, i) => (
          <Item key={i}>
            <ItemTitle>{job.company}</ItemTitle>
            <ItemSubTitle>
              {job.position} • {job.duration}
            </ItemSubTitle>
            <ItemDescription>{job.summary}</ItemDescription>

            {job.achievements && <List items={job.achievements} />}
          </Item>
        ))}
      </Section>

      <Section>
        <SectionTitle>Education</SectionTitle>
        {data.education.map((institution, i) => (
          <Item key={i}>
            <ItemTitle>{institution.school}</ItemTitle>
            <ItemSubTitle>
              {institution.faculty} • {institution.duration}
            </ItemSubTitle>
            <ItemDescription>{institution.degree}</ItemDescription>
          </Item>
        ))}
      </Section>

      <Section>
        <SectionTitle>Courses &amp; Certifications</SectionTitle>
        {data.courses.map((course, i) => (
          <ItemCondensed key={i}>
            <ItemSubTitle>{course.name}</ItemSubTitle>
            <ItemDescription className="divide-solid">
              {course.date && <span className="mx-1">{course.date}</span>}
              {course.description && (
                <span className="mx-1">{course.description}</span>
              )}
              {course.link ? (
                <a
                  className="mx-1 cursor-pointer hover:underline text-blue-500"
                  href={course.link.href}>
                  {course.link.text}
                </a>
              ) : null}
            </ItemDescription>
          </ItemCondensed>
        ))}
      </Section>
    </Layout>
  )
}

export default IndexPage
