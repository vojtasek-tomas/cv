const CompressionPlugin = require('compression-webpack-plugin')

const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  future: {
    webpack5: true
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack']
    })

    if (isProd) {
      config.plugins.push(
        new CompressionPlugin({
          filename: '[path][base].gz',
          algorithm: 'gzip'
        })
      )
      config.plugins.push(
        new CompressionPlugin({
          filename: '[path][base].br',
          algorithm: 'brotliCompress'
        })
      )
    }

    config.module.rules.push({
      test: /\.jpg$/,
      type: 'asset/inline'
    })

    return config
  },
  assetPrefix: isProd ? './' : undefined
}
